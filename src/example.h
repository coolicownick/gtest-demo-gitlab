// Copyright [2021] <Nick KUl>

#ifndef SRC_EXAMPLE_H_
#define SRC_EXAMPLE_H_

double add_numbers(double f1, double f2);

double subtract_numbers(double f1, double f2);

double multiply_numbers(double f1, double f2);

#endif  // SRC_EXAMPLE_H_
