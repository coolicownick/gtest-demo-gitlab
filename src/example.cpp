// Copyright [2021] <Nick KUl>
#include "example.h"

auto add_numbers(const double f1, const double f2) -> double { return f1 + f2; }

auto subtract_numbers(const double f1, const double f2) -> double {
  return f1 - f2;
}

auto multiply_numbers(const double f1, const double f2) -> double {
  return f1 * f2;
}
